package com.packt.dziekanat.repository;

import com.packt.dziekanat.domain.Student;

import java.util.List;

public interface StudentRepo {
    public List<Student> getStudentList();
}
