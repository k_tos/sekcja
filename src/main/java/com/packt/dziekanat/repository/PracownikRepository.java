package com.packt.dziekanat.repository;

import com.packt.dziekanat.domain.Pracownik;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kamil
 */
@Repository
public class PracownikRepository implements PracownikRepo {
    private List<Pracownik> pracownikList = new ArrayList<>();

    public PracownikRepository() {
        Pracownik pracownik1 = new Pracownik("222", "Pani", "Krysia","Bezużyteczna egzystencja");
        Pracownik pracownik2 = new Pracownik("333", "Ania", "Penera","Brak");
        Pracownik pracownik3 = new Pracownik("456", "Adam", "Jagielończyk","Dziekan");

        pracownikList.add(pracownik1);
        pracownikList.add(pracownik2);
        pracownikList.add(pracownik3);
    }

    public List<Pracownik> getPracownikList() {
        return pracownikList;
    }

    public void setPracownikList(List<Pracownik> pracownikList) {
        this.pracownikList = pracownikList;
    }
}
