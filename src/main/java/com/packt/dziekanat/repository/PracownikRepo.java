package com.packt.dziekanat.repository;

import com.packt.dziekanat.domain.Pracownik;

import java.util.List;

public interface PracownikRepo {
    List<Pracownik> getPracownikList();
}
