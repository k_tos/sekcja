package com.packt.dziekanat.repository;

import com.packt.dziekanat.domain.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository implements StudentRepo{
    private List<Student> studentList = new ArrayList<>();

    public StudentRepository() {

        Student student1 = new Student("1", "Zdzisław", "Rembajło", 123);
        Student student2 = new Student("2", "Kazimierz", "jagiełło", 345);
        Student student3 = new Student("3", "Anna", "Gonera xD", 789);

        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}