package com.packt.dziekanat.domain;

public class Student {
    private String studentId;
    private String imie;
    private String nazwisko;
    private Integer numerIndeksu;

    public Student(String studentId, String imie, String nazwisko, Integer numerIndeksu) {
        this.studentId = studentId;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerIndeksu = numerIndeksu;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Integer getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(Integer numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

}
