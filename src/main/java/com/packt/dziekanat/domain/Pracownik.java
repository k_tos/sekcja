package com.packt.dziekanat.domain;

/**
 * @author Kamil
 */
public class Pracownik {
    private String pracownikId;
    private String imie;
    private String nazwisko;
    private String funkcja;

    public Pracownik(String pracownikId, String imie, String nazwisko, String funkcja) {
        this.pracownikId = pracownikId;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.funkcja = funkcja;
    }

    public String getPracownikId() {
        return pracownikId;
    }

    public void setPracownikId(String pracownikId) {
        this.pracownikId = pracownikId;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getFunkcja() {
        return funkcja;
    }

    public void setFunkcja(String funkcja) {
        this.funkcja = funkcja;
    }
}
