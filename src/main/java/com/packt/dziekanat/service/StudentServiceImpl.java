package com.packt.dziekanat.service;

import com.packt.dziekanat.domain.Student;
import com.packt.dziekanat.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepo studentRepo;

    public List<Student> getAllStudents() {
        return studentRepo.getStudentList();
    }

    public List<Student> getAllStudentsUpperCase() {
        return listToUpperCase(studentRepo.getStudentList());
    }

    public List<Student> listToUpperCase(List<Student> list) {
        list.forEach(pracownik -> {
            pracownik.setImie(pracownik.getImie().toUpperCase());
            pracownik.setNazwisko(pracownik.getNazwisko().toUpperCase());
        });
        return list;
    }
}
