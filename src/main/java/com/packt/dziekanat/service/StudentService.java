package com.packt.dziekanat.service;

import com.packt.dziekanat.domain.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();
    List<Student> getAllStudentsUpperCase();
}
