package com.packt.dziekanat.service;

import com.packt.dziekanat.domain.Pracownik;

import java.util.List;

public interface PracownikService {
    List<Pracownik> getPracownikList();
    Pracownik getPracownikById(String id);
}
