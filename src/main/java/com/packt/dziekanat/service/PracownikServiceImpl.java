package com.packt.dziekanat.service;

import com.packt.dziekanat.domain.Pracownik;
import com.packt.dziekanat.repository.PracownikRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Kamil
 */
@Service
public class PracownikServiceImpl implements PracownikService {

    @Autowired
    PracownikRepo pracownikRepo;

    public List<Pracownik> getPracownikList() {
        return pracownikRepo.getPracownikList();
    }

    @Override
    public Pracownik getPracownikById(String id) {

        return pracownikRepo.getPracownikList().stream()
                .filter(pracownik -> pracownik.getPracownikId().equals(id))
                .findFirst()
                .orElse(null);

    }
}
