package com.packt.dziekanat.controller;

import com.packt.dziekanat.service.PracownikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Kamil
 */
@Controller
public class PracownikController {

    @Autowired
    PracownikService pracownikService;

    @RequestMapping("/employees")
    public String pracownikList(Model model) {
        model.addAttribute("title", "Pracownicy");
        model.addAttribute("greeting", "Lista Pracowników");
        model.addAttribute("tagline", "Wszyscy pracownicy");
        model.addAttribute("listOfItems", pracownikService.getPracownikList());
        return "tableView";
    }

    @RequestMapping("/employees/{pracownikId}")
    public String pracownikById(@PathVariable("pracownikId") String id, Model model) {
        model.addAttribute("title", "Szczegóły");
        model.addAttribute("greeting", "Pracownik: " + id);
        model.addAttribute("tagline", "Szczegóły pracownika");
        model.addAttribute("pracownik", pracownikService.getPracownikById(id));
        return "employeeDetail";
    }
}
