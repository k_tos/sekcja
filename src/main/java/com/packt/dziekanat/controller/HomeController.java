package com.packt.dziekanat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping({"/home", "/"})
    public String welcome(Model model) {
        model.addAttribute("title", "Dziekanat");
        model.addAttribute("greeting", "Turbo-dziekanat");
        model.addAttribute("tagline", "Politechnika Krakowska");
        return "welcome";
   }


}
