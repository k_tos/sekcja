package com.packt.dziekanat.controller;

import com.packt.dziekanat.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Kamil
 */
@Controller
public class StudentTableController {

    @Autowired
    private StudentService studentService;

//    @RequestMapping("/students")
    public String welcome(Model model) {
        model.addAttribute("title", "Lista Studentów");
        model.addAttribute("greeting", "Studenci");
        model.addAttribute("tagline", "Lista wszystkich studentów");
        model.addAttribute("listOfItems", studentService.getAllStudentsUpperCase());

        return "tableView";
    }
}
