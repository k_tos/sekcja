<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>${title}</title>
</head>
<body>
    <section>
        <div class="jumbotron">
            <div class="container">
                <h1> ${greeting} </h1>
                <p> ${tagline} </p>
            </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Id Pracownika</th>
                        <th>Imie i Nazwisko</th>
                        <th>Funkcja</th>
                        <th>Akcja</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${listOfItems}" var="item" varStatus="loopCounter">
                        <tr>
                            <td>${loopCounter.count}</td>
                            <td>${item.pracownikId}</td>
                            <td>${item.imie} ${item.nazwisko}</td>
                            <td>${item.funkcja}</td>
                            <td>
                                <a href="<spring:url value="/employees/${item.pracownikId}"/>" class="btn btn-primary">Szczegóły</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>
