<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>${title}</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1> ${greeting} </h1>
            <p> ${tagline} </p>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="card-header">Sczegóły pracownika</div>
                    <div class="card-text">
                        <h2>Pracownik id: ${pracownik.pracownikId}</h2>
                        <p>Imie i nazwisko: ${pracownik.imie} ${pracownik.nazwisko}</p>
                        <p>Funkcja: ${pracownik.funkcja}</p>
                    </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
